#include <system.h>
#include "gpio.h"

typedef struct {

} stm_gpio_t;

static GPIO_TypeDef* to_stm_gpio(ze_u8_t pin)
{
    if (pin >= PA0 && pin <= PA15)
        return GPIOA;
    else if (pin >= PB0 && pin <= PB15)
        return GPIOB;
    else if (pin >= PC0 && pin <= PC15)
        return GPIOC;
    else if (pin >= PD0 && pin <= PD15)
        return GPIOD;
    else if (pin >= PE0 && pin <= PE15)
        return GPIOE;
    return NULL;
}

static void stm_gpio_clock_enable(ze_u8_t pin)
{
    ze_u32_t portbase = (ze_u32_t)to_stm_gpio(pin);
    switch(portbase)
    {
    case GPIOA_BASE:
      __HAL_RCC_GPIOA_CLK_ENABLE();
      break;
    case GPIOB_BASE:
      __HAL_RCC_GPIOB_CLK_ENABLE();
      break;
#if defined(GPIOC_BASE)
    case GPIOC_BASE:
      __HAL_RCC_GPIOC_CLK_ENABLE();
      break;
#endif
#if defined(GPIOD_BASE)
    case GPIOD_BASE:
      __HAL_RCC_GPIOD_CLK_ENABLE();
      break;
#endif
#if defined(GPIOE_BASE)
    case GPIOE_BASE:
      __HAL_RCC_GPIOE_CLK_ENABLE();
      break;
#endif
#if defined(GPIOF_BASE) 
    case GPIOF_BASE:
      __HAL_RCC_GPIOF_CLK_ENABLE();
      break;
#endif
#if defined(GPIOG_BASE)
    case GPIOG_BASE:
      __HAL_RCC_GPIOG_CLK_ENABLE();
      break;
#endif
#if defined(GPIOH_BASE)
    case GPIOH_BASE:
      __HAL_RCC_GPIOH_CLK_ENABLE();
      break;
#endif
#if defined(GPIOI_BASE)
    case GPIOI_BASE:
      __HAL_RCC_GPIOI_CLK_ENABLE();
      break;
#endif   
#if defined(GPIOJ_BASE)
    case GPIOJ_BASE:
      __HAL_RCC_GPIOJ_CLK_ENABLE();
      break;
#endif
#if defined(GPIOK_BASE)
    case GPIOK_BASE:
      __HAL_RCC_GPIOK_CLK_ENABLE();
      break;
#endif
    default:
      print("can not find this port!");
      break;
    }
}

static void stm_gpio_config(ze_u8_t pin, ze_u32_t dir)
{
    GPIO_TypeDef* gpio = to_stm_gpio(pin);

    GPIO_InitTypeDef  GPIO_InitStruct;

    stm_gpio_clock_enable(pin);

    GPIO_InitStruct.Pin = (1 << (pin % 16));
    if (dir == GPIO_DIR_INPUT)
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    else
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
        
    HAL_GPIO_Init(gpio, &GPIO_InitStruct);
}

static int stm_gpio_write_pin(gpiochip_t* chip, ze_u8_t pin, gpio_level_t level)
{
    GPIO_TypeDef* gpio = to_stm_gpio(pin);

    if (level == GPIO_LEVEL_LOW)
    {
        HAL_GPIO_WritePin(gpio, (1 << (pin % chip->port_pins)), GPIO_PIN_RESET);
    }
    else
    {
        HAL_GPIO_WritePin(gpio, (1 << (pin % chip->port_pins)), GPIO_PIN_SET);
    }
    return 0;
}

static int stm_gpio_read_pin(gpiochip_t* chip, ze_u8_t pin)
{
    GPIO_TypeDef* gpio = to_stm_gpio(pin);

    if(HAL_GPIO_ReadPin(gpio, (1 << (pin % chip->port_pins))) == GPIO_PIN_RESET)
        return GPIO_LEVEL_LOW;
    else
        return GPIO_LEVEL_HIGH;
}

static int stm_gpio_write_pin_dir(gpiochip_t* chip, ze_u8_t pin, gpio_dir_t dir)
{
    stm_gpio_config(pin, dir);
    return 0;
}

static int stm_gpio_read_pin_dir(gpiochip_t* chip, ze_u8_t pin)
{

    return 0;
}

const gpiochip_ops_t stm_gpio_ops = {
    .read_pin = stm_gpio_read_pin,
    .write_pin = stm_gpio_write_pin,
    .read_pin_dir = stm_gpio_read_pin_dir,
    .write_pin_dir = stm_gpio_write_pin_dir,
};

void user_gpio_init(gpiochip_t* chip)
{
    gpiochip_init(chip, 16, &stm_gpio_ops);
}