#!/bin/bash

project_dir=$(cd "$(dirname "$0")"; 'pwd')
echo "project_dir=$project_dir"
zero_workspace=$(cd "$project_dir/.."; 'pwd')
zero_sdk_name="zero-mcu"
build_dir=$project_dir/build
build_type="Debug"
sync_type="none"


clean_project()
{
    echo "clean project"
    rm -rf $build_dir
    exit 1
}

Usage()
{
    echo "----------------------------------------
Usage:
    sh build.sh [debug|release] --sync
    sh build.sh clean
------------------------------------------"
    exit 1
}

if [ "$#" -eq 1 ] && [ "$1" = "-h" ]; then
    Usage
elif [ "$#" -ge 1 ]; then

    if [ "$1" = "debug" ]; then
        build_type="Debug"
    elif [ "$1" = "release" ]; then
        build_type=$1
    elif [ "$1" = "clean" ]; then
        clean_project
    else
        Usage
    fi
fi

PROJECT_PATH=$(cd "$(dirname "$0")"; pwd)
ZERO_MCU_HOME="$PROJECT_PATH/../.."
source $ZERO_MCU_HOME/scripts/env.sh

build_dir=$PROJECT_PATH/build

build_project()
{
    echo "build project"
    mkdir -p $build_dir
    cd $build_dir

    if [ $build_type = "Debug" ]; then
        cmake -DCMAKE_BUILD_TYPE="Debug" -DZERO_SDK_PATH=$ZERO_SDK_PATH -DCMAKE_TOOLCHAIN_FILE=$ZERO_STM32_CMAKE_TOOLCHAIN_FILE -DSTM32Cube_DIR=${ZERO_3RD_PATH}/STM32Cube_FW_F1_V1.7.0 -DTOOLCHAIN_PREFIX=$ZERO_GCC_TOOLCHAIN_PREFIX $PROJECT_PATH
    elif [ $build_type = "Release" ]; then
        cmake -DCMAKE_BUILD_TYPE="Release" -DZERO_SDK_PATH=$ZERO_SDK_PATH -DCMAKE_TOOLCHAIN_FILE=$ZERO_STM32_CMAKE_TOOLCHAIN_FILE -DSTM32Cube_DIR=${ZERO_3RD_PATH}/STM32Cube_FW_F1_V1.7.0 -DTOOLCHAIN_PREFIX=$ZERO_GCC_TOOLCHAIN_PREFIX $PROJECT_PATH
    fi
    make
}
build_project



