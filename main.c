#include <system.h>
#include "driver/uart.h"
#include "driver/clock.h"
#include "driver/gpio.h"

static uart_t uart0;
static gpiochip_t stm_gpio;
static gpio_t led0;

int user_terminal_putchar(int ch)
{
    return uart_write(&uart0, (const ze_u8_t*)&ch, 1);
}

int user_terminal_getchar(void)
{
    int ch = 0;
    if (uart_read(&uart0, (ze_u8_t*)&ch, 1) <= 0)
        return -1;
    return ch;
}

static stdio_t stdio = {
    .put_char = user_terminal_putchar,
    .get_char = user_terminal_getchar,
};

static void led_server(void const* arg)
{
    for (;;)
    {
        gpio_write(&led0, GPIO_LEVEL_HIGH);
        sleep_ms(500);
        gpio_write(&led0, GPIO_LEVEL_LOW);
        sleep_ms(500);
    }
}

static void led_server_init(void)
{
    osThreadDef(led, led_server, osPriorityNormal, 0, configMINIMAL_STACK_SIZE);
    osThreadCreate(osThread(led), NULL);
}

void driver_init(void)
{
    clock_init();
    user_uart_init(UART_0, &uart0, 115200);
    terminal_init(&stdio);
    user_gpio_init(&stm_gpio);

    gpio_init(&led0, &stm_gpio, PB5);
    gpio_write_dir(&led0, GPIO_DIR_OUTPUT);

    print("driver init completed.");
}

void application_init(void)
{

    led_server_init();
    print("application init completed.");
}

int main(void)
{
    driver_init();
    application_init();
    system_schedule();
    return 0;
}